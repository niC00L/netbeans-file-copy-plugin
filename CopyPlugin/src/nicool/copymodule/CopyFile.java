/*
 * created by niC00L
 */
package nicool.copymodule;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.text.StyledDocument;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

@ActionID(
		category = "Tools",
		id = "nicool.copymodule.CopyFile"
)
@ActionRegistration(
		displayName = "#CTL_CopyFile"
)
@ActionReferences({
	@ActionReference(path = "Menu/Tools", position = 0, separatorBefore = -50, separatorAfter = 50),
	@ActionReference(path = "Shortcuts", name = "OS-SLASH")
})
@Messages("CTL_CopyFile=niC00L's magic copy")
public final class CopyFile implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		copyToDest();
	}

	public void copyToDest() {
		String copyFrom = "vendor/wame";
		String copyTo = "app";
		TopComponent activeTC = TopComponent.getRegistry().getActivated();
		DataObject dataLookup = activeTC.getLookup().lookup(DataObject.class);
		String src = dataLookup.getPrimaryFile().getPath();
		String dest = "You are in wrong directory";
		if (src.contains(copyFrom)) {
			dest = src.replaceFirst(copyFrom, copyTo);
			Path srcFile = Paths.get(src);
			Path destFile = Paths.get(dest);
			this.copyFile(srcFile, destFile);
		} else {
			DialogDisplayer.getDefault().notify(new NotifyDescriptor.Message(dest, NotifyDescriptor.INFORMATION_MESSAGE));
		}
	}
	
	private void openFile(String src) {
		File f = new File(src);
		FileObject fo = FileUtil.fromFile(f)[0];
		try {
			DataObject d = DataObject.find(fo);
			EditorCookie ec = (EditorCookie) d.getLookup().lookup(EditorCookie.class);
			ec.open();
			StyledDocument doc = ec.openDocument();
		} catch (IOException ex) {
			DialogDisplayer.getDefault().notify(new NotifyDescriptor.Message(ex.toString(), NotifyDescriptor.ERROR_MESSAGE));
		}
	}

	private void copyFile(Path srcFile, Path destFile) {
		Path destPath = destFile.getParent();

		if (Files.exists(destFile)) {
			DialogDisplayer.getDefault().notify(new NotifyDescriptor.Message("File already exists", NotifyDescriptor.INFORMATION_MESSAGE));
			this.openFile(destFile.toString());
		} else {
			try {
				Files.createDirectories(destPath);
				Files.copy(srcFile, destFile);
				this.openFile(destFile.toString());
			} catch (IOException ex) {
				DialogDisplayer.getDefault().notify(new NotifyDescriptor.Message(ex.toString(), NotifyDescriptor.ERROR_MESSAGE));
			}
		}
	}
}
