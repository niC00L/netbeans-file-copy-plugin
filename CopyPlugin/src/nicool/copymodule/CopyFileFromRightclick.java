/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nicool.copymodule;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import org.openide.loaders.DataObject;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;


@ActionID(
		category = "File",
		id = "nicool.copymodule.CopyFileFromRightclick"
)
@ActionRegistration(
		displayName = "#CTL_CopyFileFromRightclick"
)
@ActionReference(path = "Loaders/Languages/Actions", position = 1400, separatorBefore = 1350)
@Messages("CTL_CopyFileFromRightclick=Copy File To App")
public final class CopyFileFromRightclick implements ActionListener {

	private final List<DataObject> context;

	public CopyFileFromRightclick(List<DataObject> context) {
		this.context = context;
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		for (DataObject dataObject : context) {
			CopyFile copy = new CopyFile();
			copy.copyToDest();
		}
	}
}
