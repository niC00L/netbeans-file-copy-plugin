# README #

This is plugin created for copying files and their parent folders

### How it works? ###

* Open any file in "vendor/wame"
* Go to Tools and click "niC00l's magic copy" (or just press Alt+Shift+/)
* File will be copied to "app" folder with all parent folders
* Copied file will be opened so you can start working right away!

### Some other info ###

* If file exists, it won't be copied, but it will be opened
* You can copy folders as well, they will be empty
* You can not copy multiple files/folders (yet)

### How do I get set up? ###

* Download .npm file from /dist folder
* Install in NetBeans

*Note: You need Java version 1.8 or higher*